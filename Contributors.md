# Contribuidores

- [Isabella Breder](https://gitlab.com/isabellabreder)
- [Henrique F. Simões](https://gitlab.com/henriquesimoes)
- [Gabriela Souza](https://gitlab.com/gabivsouza)
- [Valentina Spohr](https://gitlab.com/valentina.spohr)
- [Henrique Parede](https://gitlab.com/henriquehpds)
- [Gustavo Sousa](https://gitlab.com/gugugupavo)
